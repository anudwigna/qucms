export const state = () => ({
    jsonContent: '',
    htmlContent:''
})
  
export const mutations = {
    setHtmlContent (state, htmlContent) {
        state.htmlContent = htmlContent;
    },
    setJsonContent (state, jsonContent) {
        state.jsonContent = jsonContent;
    }
}