export const state = () => ({
    message: '',
    show:false,
    progressColor:'red'
})
  
export const mutations = {
    setMessage (state, message) {
        state.message = message;
    },
    setProgressColor (state, color) {
        state.progressColor = color;
    }
}