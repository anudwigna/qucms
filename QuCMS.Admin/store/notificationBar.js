export const state = () => ({
    notification: ''
})
  
export const mutations = {
    setNotification (state, snack) {
        state.notification = snack
    }
}