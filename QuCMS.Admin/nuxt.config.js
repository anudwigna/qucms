const pkg = require('./package')


const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
  mode: 'spa',
  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0', // default: localhost
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '~/assets/style/croppa.min.css',
    '~/assets/style/editor.css',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/apollo'
  ],
  /*
  ** Axios module configuration
  */
  apollo:{
    tokenName: 'auth._token.local',
    authenticationType: '',
    clientConfigs:{
      default:{
        //httpEndpoint: 'http://localhost:50425/graphql'
        httpEndpoint: 'https://qucms.qubex.com.np/graphql'
        //httpEndpoint: 'http://182.93.85.219:8041/graphql'
      }
    }
  },

  axios: {
    //baseURL: 'http://localhost:50425/api/'
    baseURL: 'https://qucms.qubex.com.np/api/'
    //baseURL: 'http://182.93.85.219:8041/api/'
  },
  
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'auth/login', method: 'post', propertyName: 'accessToken' },
          user: { url: 'auth/user', method: 'get', propertyName: 'result' },
          logout: false
        }
      }
    },
    redirect: {
      login: '/login',
      logout: '/login',
      user: '/profile',
      callback:'/callback'
    }
  },

  router: {
    middleware: ['auth']
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ["~assets/style/variables.styl"]
      }
    },
    
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.resolve.alias['vue'] = 'vue/dist/vue.common'
    }
  }
}
