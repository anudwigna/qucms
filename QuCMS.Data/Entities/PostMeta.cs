﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class PostMeta
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string AdminName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public Post Post { get; set; }
    }
}
