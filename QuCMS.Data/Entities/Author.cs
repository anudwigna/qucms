﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class Author
    {
        public Author()
        {
            PostAuthor = new HashSet<PostAuthor>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public ICollection<PostAuthor> PostAuthor { get; set; }
    }
}
