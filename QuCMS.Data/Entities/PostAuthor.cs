﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class PostAuthor
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int AuthorId { get; set; }

        public Author Author { get; set; }
        public Post Post { get; set; }
    }
}
