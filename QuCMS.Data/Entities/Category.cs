﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class Category
    {
        public Category()
        {
            Post = new HashSet<Post>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public ICollection<Post> Post { get; set; }
    }
}
