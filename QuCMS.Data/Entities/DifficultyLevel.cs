﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class DifficultyLevel
    {
        public DifficultyLevel()
        {
            Post = new HashSet<Post>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public ICollection<Post> Post { get; set; }
    }
}
