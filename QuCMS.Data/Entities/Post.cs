﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class Post
    {
        public Post()
        {
            PostAuthor = new HashSet<PostAuthor>();
            PostMeta = new HashSet<PostMeta>();
            PostTag = new HashSet<PostTag>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Excerpt { get; set; }
        public string HtmlContent { get; set; }
        public string JsonContent { get; set; }
        public int CategoryId { get; set; }
        public int DifficultyLevelId { get; set; }
        public string MainImage { get; set; }
        public bool IsActive { get; set; }
        public DateTime PublishedDate { get; set; }
        public string AdminName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public Category Category { get; set; }
        public DifficultyLevel DifficultyLevel { get; set; }
        public ICollection<PostAuthor> PostAuthor { get; set; }
        public ICollection<PostMeta> PostMeta { get; set; }
        public ICollection<PostTag> PostTag { get; set; }
    }
}
