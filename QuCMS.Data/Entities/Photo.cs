﻿using System;
using System.Collections.Generic;

namespace QuCMS.Data.Entities
{
    public partial class Photo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Extension { get; set; }
        public string AdminName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Title { get; set; }
    }
}
