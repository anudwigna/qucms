﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuCMS.Data.AuthModels
{
    public class AuthRelated
    {
    }

    public class LoginVM
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class RegisterVM
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
