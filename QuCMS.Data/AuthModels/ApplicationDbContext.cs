﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QuCMS.Data.AuthModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuCMS.Data.AuthModels
{
    public class ApplicationDbContext
          : IdentityDbContext<ApplicationUser, ApplicationUserRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }
    }
}
