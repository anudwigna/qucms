﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuCMS.Data.Entities
{
    public partial class Photo
    {
        [NotMapped]
        public string ImageUrl { get; set; }
    }
}
