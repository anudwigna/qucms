﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuCMS.Data.Entities
{
    public partial class Post
    {
        [NotMapped]
        public IEnumerable<Author> Authors { get; set; }

        [NotMapped]
        public IEnumerable<Tag> Tags { get; set; }

        [NotMapped]
        public IEnumerable<PostMeta> Metas { get; set; }
    }
}
