﻿using Microsoft.EntityFrameworkCore;
using QuCMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QuCMS.Data.Repositories
{
    public interface ICategoryRepository
    {
        Task<Category> GetById(int id);
        Task<IEnumerable<Category>> GetAll();
    }


    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _context;

        public CategoryRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Category> GetById(int id)
        {
            return await _context.Category.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await _context.Category.ToListAsync();
        }
    }
}
