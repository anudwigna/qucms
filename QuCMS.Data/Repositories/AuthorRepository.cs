﻿using QuCMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace QuCMS.Data.Repositories
{
    public interface IAuthorRepository
    {
        Task<Author> GetById(int id);
        Task<IEnumerable<Author>> GetAll();
    }

    public class AuthorRepository : IAuthorRepository
    {
        private readonly AppDbContext _context;

        public AuthorRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Author> GetById(int id)
        {
            return await _context.Author.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<IEnumerable<Author>> GetAll()
        {
            return await _context.Author.ToListAsync();
        }
    }
}
