﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using QuCMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using AngleSharp.Html.Parser;
using AngleSharp.Dom;

namespace QuCMS.Data.Repositories
{
    public interface IPostRepository
    {
        Task<Post> Insert(Post post);
        Task<Post> Update(Post post);
        Task Delete(Post post);
        Task<Post> GetById(int id);
        Task<IEnumerable<Post>> GetAll();
        Task<IEnumerable<Post>> GetLatest();
        Task<bool> Exists(int id);
        Task<bool> CheckIfSlugExists(string slug);

        //post meta related functions
        Task<IEnumerable<PostMeta>> GetMetaByPostId(int id);
    }

    public class PostRepository : IPostRepository
    {
        private AppDbContext _context;
        private IHttpContextAccessor _httpContext;

        public PostRepository(AppDbContext context, IHttpContextAccessor httpContext)
        {
            _context = context;
            _httpContext = httpContext;
        }

        public async Task Delete(Post post)
        {
            var toBeDeletedItem = await _context.Post.FirstOrDefaultAsync(i => i.Id == post.Id);
            _context.Remove(toBeDeletedItem);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> Exists(int id)
        {
            return await _context.Post.AnyAsync(i => i.Id == id);
        }

        public async Task<IEnumerable<Post>> GetAll()
        {
            try
            {
                var posts = await _context.Post
                .Include(p => p.Category)
                .Include(x => x.Category)
                .Include(x => x.DifficultyLevel)
                .Include(x => x.PostAuthor).ThenInclude(pa => pa.Author)
                .Include(x => x.PostTag).ThenInclude(pt => pt.Tag)
                .Include(x => x.PostMeta)
                .OrderByDescending(p => p.Id)
                .ToListAsync();

                foreach(var post in posts)
                {
                    List<Author> authors = new List<Author>();
                    List<Tag> tags = new List<Tag>();
                    foreach (var item in post.PostAuthor)
                    {
                        authors.Add(item.Author);
                    }
                    post.Authors = authors;

                    foreach (var item in post.PostTag)
                    {
                        tags.Add(item.Tag);
                    }
                    post.Tags = tags;
                }

                return posts;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Post> GetById(int id)
        {
            List<Author> authors = new List<Author>();
            List<Tag> tags = new List<Tag>();
            var post =  await _context.Post
                .Include(x => x.Category)
                .Include(x => x.DifficultyLevel)
                .Include(x => x.PostAuthor).ThenInclude(pa => pa.Author)
                .Include(x => x.PostTag).ThenInclude(pt => pt.Tag)
                .Include(x => x.PostMeta)
                .FirstOrDefaultAsync(i => i.Id == id);

            foreach (var item in post.PostAuthor)
            {
                authors.Add(item.Author);
            }

            foreach (var item in post.PostTag)
            {
                tags.Add(item.Tag);
            }
            post.Authors = authors;
            post.Tags = tags;
            return post;
        }

        public async Task<Post> Insert(Post post)
        {
            try
            {
                if (await _context.Post.AnyAsync(p => p.Title.ToLower() == post.Title.ToLower()))
                    throw new Exception("The Post with this title already Exists!");
                if (await _context.Post.AnyAsync(p => p.Slug.ToLower() == post.Slug.ToLower()))
                    throw new Exception("The Post with this slug already Exists!");
                var claims = _httpContext.HttpContext.User.Claims;
                string adminName = claims.Where(c => c.Type == "sub").FirstOrDefault().Value;
                post.AdminName = adminName;
                post.Category = await _context.Category.FirstOrDefaultAsync(c => c.Id == post.Category.Id);
                post.DifficultyLevel = await _context.DifficultyLevel.FirstOrDefaultAsync(c => c.Id == post.DifficultyLevel.Id);
                post.CreatedDate = DateTime.UtcNow.AddMinutes(345);
                TimeSpan currentTime = post.CreatedDate.TimeOfDay;
                post.PublishedDate = post.PublishedDate.Add(currentTime);
                post.IsActive = true;
                post.MainImage = await GetMainImage(post.HtmlContent);

                //adding v-img and target in anchors
                post.HtmlContent = await RefineHTMLContent(post.HtmlContent);

                //Adding Authors
                foreach(var author in post.Authors)
                {
                    var _tempAuthor = await _context.Author.FirstOrDefaultAsync(a => a.Id == author.Id);
                    await _context.PostAuthor.AddAsync(new PostAuthor() { Post = post, Author = _tempAuthor });
                }

                //Adding Tags
                foreach (var tag in post.Tags)
                {
                    var _tempTag = await _context.Tag.FirstOrDefaultAsync(t => t.Id == tag.Id);
                    await _context.PostTag.AddAsync(new PostTag() { Post = post, Tag = _tempTag });
                }

                //Adding Meta Information
                foreach (var meta in post.Metas)
                {
                    await _context.PostMeta.AddAsync(
                        new PostMeta()
                        {
                            Post = post, Name = meta.Name, Content = meta.Content,
                            CreatedDate = post.CreatedDate,
                            AdminName = post.AdminName,
                        });
                }

                _context.Post.Add(post);
                _context.SaveChanges();
                return post;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return new Post();
            }
        }

        private async Task<string> GetMainImage(string htmlContent)
        {
            var parser = new HtmlParser();
            var document = await parser.ParseDocumentAsync(htmlContent);
            var firstImage = document.All.FirstOrDefault(x => x.LocalName == "img");
            if(firstImage != null)
            {
                var theArray = firstImage.Attributes["src"].Value.Split('/');
                return theArray[4] ;
            }
            else
            {
                return null;
            }
        }

        public async Task<Post> Update(Post post)
        {
            try
            {
                if (_context.Post.Any(p => p.Slug == post.Slug && p.Id != post.Id))
                {
                    throw new Exception("This slug already exists!");
                }

                var toBeUpdatedItem = await _context.Post.FirstOrDefaultAsync(i => i.Id == post.Id);
                var claims = _httpContext.HttpContext.User.Claims;
                string adminName = claims.Where(c => c.Type == "sub").FirstOrDefault().Value;
                toBeUpdatedItem.AdminName = adminName;
                toBeUpdatedItem.ModifiedDate = DateTime.UtcNow.AddMinutes(345);
                toBeUpdatedItem.Title = post.Title;
                toBeUpdatedItem.Slug = post.Slug;
                toBeUpdatedItem.Excerpt = post.Excerpt;
                toBeUpdatedItem.JsonContent = post.JsonContent;
                
                toBeUpdatedItem.Category = await _context.Category.FirstOrDefaultAsync(c => c.Id == post.Category.Id);
                toBeUpdatedItem.DifficultyLevel = await _context.DifficultyLevel.FirstOrDefaultAsync(c => c.Id == post.DifficultyLevel.Id);
                toBeUpdatedItem.MainImage = await GetMainImage(post.HtmlContent);

                //refining images and anchors
                toBeUpdatedItem.HtmlContent = await RefineHTMLContent(post.HtmlContent);

                var currentDateTimeSpan = DateTime.UtcNow.AddMinutes(345).TimeOfDay;
                toBeUpdatedItem.PublishedDate = post.PublishedDate.Add(currentDateTimeSpan);

                #region Update Authors

                var dbPostAuthors = _context.PostAuthor.Where(pa => pa.PostId == post.Id);
                foreach (var item in post.Authors)
                {
                    if (!dbPostAuthors.Any(x => x.AuthorId == item.Id))
                    {
                        var _tempAuthor = await _context.Author.FirstOrDefaultAsync(a => a.Id == item.Id);
                        var _tempPA = new PostAuthor() { Post = post, Author = _tempAuthor };
                        await _context.PostAuthor.AddAsync(_tempPA);
                    }
                }

                foreach (var item in dbPostAuthors)
                {
                    if (!post.Authors.Any(a => a.Id == item.AuthorId))
                    {
                        var _tempPA = await _context.PostAuthor.FirstOrDefaultAsync(pa => pa.AuthorId == item.AuthorId && pa.PostId == post.Id);
                        _context.PostAuthor.Remove(_tempPA);
                    }
                }

                #endregion

                #region Update Tags

                var dbPostTags = _context.PostTag.Where(pt => pt.PostId == post.Id);
                foreach (var item in post.Tags)
                {
                    if (!dbPostTags.Any(x => x.TagId == item.Id))
                    {
                        var _tempTag = await _context.Tag.FirstOrDefaultAsync(t => t.Id == item.Id);
                        var _tempPT = new PostTag() { Post = post, Tag = _tempTag };
                        await _context.PostTag.AddAsync(_tempPT);
                    }
                }

                foreach (var item in dbPostTags)
                {
                    if (!post.Tags.Any(t => t.Id == item.TagId))
                    {
                        var _tempPT = await _context.PostTag.FirstOrDefaultAsync(pt => pt.TagId == item.TagId && pt.PostId == post.Id);
                        _context.PostTag.Remove(_tempPT);
                    }
                }

                #endregion

                #region Update Meta

                var dbPostMetas = _context.PostMeta.Where(pm => pm.PostId == post.Id);
                foreach (var item in post.Metas)
                {
                    if (!dbPostMetas.Any(x => x.Name == item.Name))
                    {
                        var _tempPM = new PostMeta() {
                            Name = item.Name,
                            Content = item.Content,
                            CreatedDate = DateTime.UtcNow.AddMinutes(345),
                            AdminName = GetAdminName()
                        };
                        await _context.PostMeta.AddAsync(_tempPM);
                    }
                    else
                    {
                        var toBeEditedPM = await _context.PostMeta.FirstOrDefaultAsync(pm => pm.Name == item.Name && pm.PostId == post.Id);
                        toBeEditedPM.ModifiedDate = DateTime.UtcNow.AddMinutes(345);
                        toBeEditedPM.Content = item.Content;
                        toBeEditedPM.AdminName = GetAdminName();
                    }
                }

                foreach (var item in dbPostMetas)
                {
                    if (!post.Metas.Any(t => t.Name == item.Name))
                    {
                        var _tempPM = await _context.PostMeta.FirstOrDefaultAsync(pm => pm.Name == item.Name && pm.PostId == post.Id);
                        _context.PostMeta.Remove(_tempPM);
                    }
                }

                #endregion

                await _context.SaveChangesAsync();
                return toBeUpdatedItem;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<Post>> GetLatest()
        {
            var posts = await _context.Post
                .Include(x => x.Category)
                .Include(x => x.DifficultyLevel)
                .Include(x => x.PostAuthor).ThenInclude(pa => pa.Author)
                .Include(x => x.PostTag).ThenInclude(pt => pt.Tag)
                .Where(p => p.IsActive == true)
                .OrderByDescending(x => x.PublishedDate)
                .Take(15)
                .ToListAsync();

            foreach (var post in posts)
            {
                List<Author> authors = new List<Author>();
                foreach (var item in post.PostAuthor)
                {
                    authors.Add(item.Author);
                }
                post.Authors = authors;
            }

            foreach (var post in posts)
            {
                List<Tag> tags = new List<Tag>();
                foreach (var item in post.PostTag)
                {
                    tags.Add(item.Tag);
                }
                post.Tags = tags;
            }
            return posts;
        }

        public async Task<bool> CheckIfSlugExists(string slug)
        {
            return await _context.Post.AnyAsync(p => p.Slug.ToLower() == slug.ToLower());
        }

        public async Task<IEnumerable<PostMeta>> GetMetaByPostId(int id)
        {
            return await _context.PostMeta.Where(pm => pm.PostId == id).ToListAsync();
        }

        private string GetAdminName()
        {
            var claims = _httpContext.HttpContext.User.Claims;
            return claims.Where(c => c.Type == "sub").FirstOrDefault().Value;
        }

        private async Task<string> RefineHTMLContent(string htmlContent)
        {
            var parser = new HtmlParser();
            var document = await parser.ParseDocumentAsync(htmlContent);

            foreach (var element in document.All)
            {
                if (element.LocalName == "img")
                {
                    if(element.Attributes["class"] == null)
                    {
                        element.SetAttribute("class", "img-responsive");
                    }
                    if(element.Attributes["alt"] == null)
                    {
                        element.SetAttribute("alt", "Article Image");
                    }
                }

                if (element.LocalName == "a")
                {
                    if (element.Attributes["target"] == null)
                    {
                        element.SetAttribute("target", "_blank");
                    }
                }
            }
            return document.FirstElementChild.OuterHtml;
        }
    }
}
