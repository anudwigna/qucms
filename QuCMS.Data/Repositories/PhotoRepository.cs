﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using QuCMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuCMS.Data.Repositories
{
    public interface IPhotoRepository
    {
        Photo Insert(Photo photo);
        Task<Photo> Update(Photo photo);
        Task Delete(Photo photo);
        Task<Photo> GetById(int id);
        Task<IEnumerable<Photo>> GetAll();
        Task<bool> Exists(int id);
        Task<IEnumerable<Photo>> Search(string query);
    }

    public class PhotoRepository : IPhotoRepository
    {
        private AppDbContext _context;
        private IHttpContextAccessor _httpContext;

        public PhotoRepository(AppDbContext context, IHttpContextAccessor httpContext)
        {
            _context = context;
            _httpContext = httpContext;
        }

        public async Task Delete(Photo photo)
        {
            var toBeDeletedItem = await _context.Photo.FirstOrDefaultAsync(i => i.Id == photo.Id);
            _context.Remove(toBeDeletedItem);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> Exists(int id)
        {
            return await _context.Photo.AnyAsync(i => i.Id == id);
        }

        public async Task<IEnumerable<Photo>> GetAll()
        {
            return await _context.Photo.ToListAsync();
        }

        public async Task<Photo> GetById(int id)
        {
            return await _context.Photo.FirstOrDefaultAsync(i => i.Id == id);
        }

        public Photo Insert(Photo photo)
        {
            
            try
            {
                var claims = _httpContext.HttpContext.User.Claims;
                string adminName = claims.Where(c => c.Type == "sub").FirstOrDefault().Value;
                photo.AdminName = adminName;
                photo.CreatedDate = DateTime.UtcNow.AddMinutes(345);
                _context.Photo.Add(photo);
                _context.SaveChanges();
                return photo;
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                return new Photo();
            }
        }

        public async Task<Photo> Update(Photo photo)
        {
            var toBeUpdatedItem = await _context.Photo.FirstOrDefaultAsync(i => i.Id == photo.Id);
            toBeUpdatedItem.AdminName = _httpContext.HttpContext.User.Identity.Name;
            toBeUpdatedItem.ModifiedDate = DateTime.UtcNow.AddMinutes(345);
            toBeUpdatedItem.Name = photo.Name;
            toBeUpdatedItem.Description = photo.Description;
            toBeUpdatedItem.Extension = photo.Extension;
            await _context.SaveChangesAsync();
            return toBeUpdatedItem;
        }

        public async Task<IEnumerable<Photo>> Search(string query)
        {
            var result =  await _context.Photo.Where(p => EF.Functions.Like(p.Description.ToLower(), $"%{query.ToLower()}%")).ToListAsync();
            foreach(var item in result)
            {
                item.ImageUrl = $"{_httpContext.HttpContext.Request.Scheme}" +
                    $"://{_httpContext.HttpContext.Request.Host}/images/{item.Name}";
            }
            return result;
        }
    }
}
