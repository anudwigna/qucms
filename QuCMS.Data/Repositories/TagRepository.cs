﻿using QuCMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace QuCMS.Data.Repositories
{
    public interface ITagRepository
    {
        Task<Tag> GetById(int id);
        Task<IEnumerable<Tag>> GetAll();
        Task<IEnumerable<Tag>> Search(string query);
    }

    public class TagRepository : ITagRepository
    {
        private readonly AppDbContext _context;

        public TagRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Tag> GetById(int id)
        {
            return await _context.Tag.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<IEnumerable<Tag>> GetAll()
        {
            return await _context.Tag.ToListAsync();
        }

        public async Task<IEnumerable<Tag>> Search(string query)
        {
            return await _context.Tag.Where(x => EF.Functions.Like(x.Name.ToLower(), $"%{query.ToLower()}%")).ToListAsync();
        }
    }


}
