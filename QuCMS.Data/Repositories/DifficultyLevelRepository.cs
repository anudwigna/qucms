﻿using QuCMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace QuCMS.Data.Repositories
{
    public interface IDifficultyLevelRepository
    {
        Task<DifficultyLevel> GetById(int id);
        Task<IEnumerable<DifficultyLevel>> GetAll();
    }

    public class DifficultyLevelRepository : IDifficultyLevelRepository
    {
        private readonly AppDbContext _context;

        public DifficultyLevelRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<DifficultyLevel> GetById(int id)
        {
            return await _context.DifficultyLevel.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<IEnumerable<DifficultyLevel>> GetAll()
        {
            return await _context.DifficultyLevel.ToListAsync();
        }
    }
}
