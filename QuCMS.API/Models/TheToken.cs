﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.Models
{
    public class TheToken
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
    }
}
