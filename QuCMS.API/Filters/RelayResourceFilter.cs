﻿using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace QuCMS.API.Filters
{
    public class RelayResourceFilter : IResourceFilter
    {
        private readonly string jsonMediaType = "application/json";

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            if (!string.Equals(MediaTypeHeaderValue.Parse(context.HttpContext.Request.ContentType).MediaType,
                this.jsonMediaType, StringComparison.OrdinalIgnoreCase))
            {
                var encoder = JavaScriptEncoder.Create();
                JObject operations = JObject.Parse(context.HttpContext.Request.Form["operations"].ToString());

                var variables = encoder.Encode(JsonConvert.SerializeObject(operations["variables"]));
                var query = encoder.Encode(JsonConvert.SerializeObject(operations["query"]));

                var body = $"{{\"query\":\"{{{query}}}\", \"variables\":\"{{{variables}}}\"}}";

                //var test = JsonConvert.SerializeObject(operations["variables"]);

                byte[] requestData = Encoding.UTF8.GetBytes(body);
                context.HttpContext.Request.Body = new MemoryStream(requestData);
                context.HttpContext.Request.ContentType = this.jsonMediaType;
            }
        }
    }
}
