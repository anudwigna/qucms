﻿using QuCMS.Data.AuthModels;
using QuCMS.Data.Entities;
//using QuCMS.Data.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuCMS.Data.Repositories;
using GraphQL;
using QuCMS.API.GraphQLRelated;
using QuCMS.API.GraphQLRelated.Types;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection.Extensions;
using GraphQL.Authorization;
using GraphQL.Validation;

namespace QuCMS.API.Extensions
{
    public static class StartupServiceExtension
    {

        public static void ConfigureDatabaseAndIdentity(this IServiceCollection services, string connectionString)
        {
            //Scaffolded Datatbase
            services.AddTransient<AppDbContext>();

            //Authentication Database
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentity<ApplicationUser, ApplicationUserRole>()
                    .AddEntityFrameworkStores<ApplicationDbContext>()
                    .AddDefaultTokenProviders();
        }

        public static void ConfigureJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = configuration["JwtIssuer"],
                        ValidAudience = configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });
        }

        public static void ConfigureGraphQL(this IServiceCollection services, Action<AuthorizationSettings> configure)
        {
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<QuCMSQuery>();
            services.AddSingleton<QuCMSMutation>();

            services.AddSingleton<CategoryType>();
            services.AddSingleton<CategoryInputType>();
            services.AddSingleton<CategoryRelatedType>();

            services.AddSingleton<AuthorType>();
            services.AddSingleton<AuthorInputType>();
            services.AddSingleton<AuthorRelatedType>();

            services.AddSingleton<DifficultyLevelType>();
            services.AddSingleton<DifficultyLevelInputType>();
            services.AddSingleton<DifficultyLevelRelatedType>();

            services.AddSingleton<TagType>();
            services.AddSingleton<TagInputType>();
            services.AddSingleton<TagRelatedType>();

            services.AddSingleton<PhotoType>();
            services.AddSingleton<PhotoRelatedType>();

            services.AddSingleton<PostType>();
            services.AddSingleton<PostInputType>();
            services.AddSingleton<PostRelatedType>();
            services.AddSingleton<PostMetaType>();
            services.AddSingleton<PostMetaInputType>();


            var sp = services.BuildServiceProvider();
            services.AddSingleton<ISchema>(new QuCMSSchema(new FuncDependencyResolver(type => sp.GetService(type))));

            //Authorization Related
            services.TryAddSingleton<IAuthorizationEvaluator, AuthorizationEvaluator>();
            services.AddTransient<IValidationRule, AuthorizationValidationRule>();
            services.TryAddSingleton(s =>
            {
                var authSettings = new AuthorizationSettings();
                configure(authSettings);
                return authSettings;
            });
        }
             
        public static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<IDifficultyLevelRepository, DifficultyLevelRepository>();
            services.AddTransient<ITagRepository, TagRepository>();
            services.AddTransient<IPhotoRepository, PhotoRepository>();
            services.AddTransient<IPostRepository, PostRepository>();
        }
    }
}