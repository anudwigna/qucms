﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated
{
    public class QuCMSSchema : Schema
    {
        public QuCMSSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<QuCMSQuery>();
            Mutation = resolver.Resolve<QuCMSMutation>();
        }
    }
}
