﻿using GraphQL.Authorization;
using GraphQL.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class DifficultyLevelRelated
    {
    }

    public class DifficultyLevelType : ObjectGraphType<DifficultyLevel>
    {
        public DifficultyLevelType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(dl => dl.Alias);
            Field(dl => dl.CreatedDate);
        }
    }

    public class DifficultyLevelInputType : InputObjectGraphType<DifficultyLevel>
    {
        public DifficultyLevelInputType()
        {
            Name = "DifficultyLevelInput";
            Field<IntGraphType>("id");
            Field<StringGraphType>("name");
        }
    }

    public class DifficultyLevelRelatedType : ObjectGraphType
    {
        public DifficultyLevelRelatedType(IDifficultyLevelRepository difficultyLevelRepository)
        {
            Name = "DifficultyLevelRelated";

            FieldAsync<DifficultyLevelType>(
                "difficultyLevel",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: async context => await difficultyLevelRepository.GetById(context.GetArgument<int>("id"))).AuthorizeWith("AdminPolicy");

            //Get All Categories
            FieldAsync<ListGraphType<DifficultyLevelType>>(
                "difficultyLevels",
                resolve: async context => await difficultyLevelRepository.GetAll());
        }
    }
}
