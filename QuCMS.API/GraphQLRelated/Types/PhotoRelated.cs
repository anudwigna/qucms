﻿using GraphQL;
using GraphQL.Authorization;
using GraphQL.Types;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class PhotoRelated
    {

    }

    public class PhotoType : ObjectGraphType<Photo>
    {
        public PhotoType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.Description);
            Field(x => x.Extension);
            Field(x => x.CreatedDate);
            Field(x => x.AdminName);
            Field(x => x.ImageUrl);
            Field(x => x.Title);
        }
    }

    public class PhotoRelatedType : ObjectGraphType
    {
        public PhotoRelatedType(IPhotoRepository photoRepository)
        {
            Name = "PhotoRelated";

            //Search Photos
            FieldAsync<ListGraphType<PhotoType>>(
                "searchPhoto",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "query" }),
                resolve: async context => await photoRepository.Search(context.GetArgument<string>("query"))).AuthorizeWith("AdminPolicy");
        }
    }
}
