﻿using GraphQL.Authorization;
using GraphQL.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class CategoryRelated
    {
    }

    public class CategoryType : ObjectGraphType<Category>
    {
        public CategoryType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.Code);
        }
    }

    public class CategoryInputType : InputObjectGraphType<Category>
    {
        public CategoryInputType()
        {
            Name = "CategoryInput";
            Field<IntGraphType>("id");
            Field<StringGraphType>("name");
        }
    }

    public class CategoryRelatedType : ObjectGraphType
    {
        public CategoryRelatedType(ICategoryRepository categoryRepository)
        {
            Name = "CategoryRelated";

            FieldAsync<CategoryType>(
                "category",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: async context => await categoryRepository.GetById(context.GetArgument<int>("id"))).AuthorizeWith("AdminPolicy");

            //Get All Categories
            FieldAsync<ListGraphType<CategoryType>>(
                "categories",
                resolve: async context => await categoryRepository.GetAll());
        }
    }
}
