﻿using GraphQL.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class PostMetaRelated
    {
    }

    public class PostMetaType : ObjectGraphType<PostMeta>
    {
        public PostMetaType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.Content);
            Field(x => x.PostId);
        }
    }

    public class PostMetaInputType : InputObjectGraphType<PostMeta>
    {
        public PostMetaInputType()
        {
            Name = "postMetaInput";
            Field<IntGraphType>("id");
            Field<StringGraphType>("name");
            Field<StringGraphType>("content");
        }
    }

    //public class PostMetaRelatedType : ObjectGraphType
    //{
    //    public PostMetaRelatedType(IPostRepository postRepository)
    //    {
    //        Name = "PostRelated";

    //        FieldAsync<AuthorType>(
    //            "author",
    //            arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
    //            resolve: async context => await authorRepository.GetById(context.GetArgument<int>("id"))).AuthorizeWith("AdminPolicy");

    //        //Get All Categories
    //        FieldAsync<ListGraphType<AuthorType>>(
    //            "authors",
    //            resolve: async context => await authorRepository.GetAll());
    //    }
    //}
}
