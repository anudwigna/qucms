﻿using GraphQL.Authorization;
using GraphQL.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class TagRelated
    {
    }

    public class TagType : ObjectGraphType<Tag>
    {
        public TagType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
        }
    }

    public class TagInputType : InputObjectGraphType<Tag>
    {
        public TagInputType()
        {
            Name = "TagInput";
            Field<IntGraphType>("id");
            Field<StringGraphType>("name");
        }
    }

    public class TagRelatedType : ObjectGraphType
    {
        public TagRelatedType(ITagRepository tagRepository)
        {
            Name = "TagRelated";

            FieldAsync<ListGraphType<TagType>>(
                "searchTag",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "name" }),
                resolve: async context => await tagRepository.Search(context.GetArgument<string>("name"))).AuthorizeWith("AdminPolicy");

            //Get All Categories
            FieldAsync<ListGraphType<TagType>>(
                "tags",
                resolve: async context => await tagRepository.GetAll());
        }
    }
}
