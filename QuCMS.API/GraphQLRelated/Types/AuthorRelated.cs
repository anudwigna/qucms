﻿using GraphQL.Authorization;
using GraphQL.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class AuthorRelated
    {
    }

    public class AuthorType : ObjectGraphType<Author>
    {
        public AuthorType()
        {
            Field(x => x.Id);
            Field(x => x.Name);
        }
    }

    public class AuthorInputType : InputObjectGraphType<Author>
    {
        public AuthorInputType()
        {
            Name = "AuthorInput";
            Field<IntGraphType>("id");
            Field<StringGraphType>("name");
        }
    }

    public class AuthorRelatedType : ObjectGraphType
    {
        public AuthorRelatedType(IAuthorRepository authorRepository)
        {
            Name = "AuthorRelated";

            FieldAsync<AuthorType>(
                "author",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: async context => await authorRepository.GetById(context.GetArgument<int>("id"))).AuthorizeWith("AdminPolicy");

            //Get All Categories
            FieldAsync<ListGraphType<AuthorType>>(
                "authors",
                resolve: async context => await authorRepository.GetAll());
        }
    }
}
