﻿using GraphQL.Authorization;
using GraphQL.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated.Types
{
    public class PostRelated
    {
    }

    public class PostType : ObjectGraphType<Post>
    {
        public PostType()
        {
            Field(x => x.Id);
            Field(x => x.Title);
            Field(x => x.Slug);
            Field(x => x.Excerpt);
            Field(x => x.HtmlContent);
            Field(x => x.JsonContent);
            Field(x => x.MainImage);
            Field(x => x.IsActive);
            Field(x => x.PublishedDate);
            Field<CategoryType>("category",
                resolve: context => context.Source.Category, description: "Post's Category");
            Field<DifficultyLevelType>("difficultyLevel",
                resolve: context => context.Source.DifficultyLevel, description: "Post's Difficulty Level");
            Field<ListGraphType<AuthorType>>("authors",
                resolve: context => context.Source.Authors, description: "Post's Authors");
            Field<ListGraphType<TagType>>("tags",
                resolve: context => context.Source.Tags, description: "Post's Tags");
            Field<ListGraphType<PostMetaType>>("metas",
                resolve: context => context.Source.PostMeta, description: "Post's Metas");
        }
    }

    public class PostInputType : InputObjectGraphType<Post>
    {
        public PostInputType()
        {
            Name = "PostInput";
            Field<IntGraphType>("id");
            Field<NonNullGraphType<StringGraphType>>("title");
            Field<NonNullGraphType<StringGraphType>>("slug");
            Field<StringGraphType>("excerpt");
            Field<StringGraphType>("htmlContent");
            Field<StringGraphType>("jsonContent");
            Field<StringGraphType>("publishedDate");
            Field<CategoryInputType>("category",
                //arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => context.Source.Category, description: "Post's Category");
            Field<DifficultyLevelInputType>("difficultyLevel",
                //arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => context.Source.DifficultyLevel, description: "Post's Difficulty Level");
            Field<ListGraphType<AuthorInputType>>("authors",
                //arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => context.Source.Authors, description: "Post's Authors");
            Field<ListGraphType<TagInputType>>("tags",
                //arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => context.Source.Tags, description: "Post's Tags");
            Field<ListGraphType<PostMetaInputType>>("metas",
                //arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => context.Source.PostMeta, description: "Post's Metas");
        }
    }

    public class PostRelatedType : ObjectGraphType
    {
        public PostRelatedType(IPostRepository postRepository)
        {
            //this.AuthorizeWith("AdminPolicy");
            Name = "PostRelated";

            FieldAsync<PostType>(
                "post",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: async context => await postRepository.GetById(context.GetArgument<int>("id")));

            FieldAsync<ListGraphType<PostType>>(
                "latestPosts",
                resolve: async context => await postRepository.GetLatest());

            FieldAsync<ListGraphType<PostType>>(
                "posts",
                resolve: async context => await postRepository.GetAll());

            //Protected Queries
            Field<PostType>(
                "insertPost",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<PostInputType>> { Name = "post" }
             ),
             resolve: context =>
             {
                 var post = context.GetArgument<Post>("post");
                 return postRepository.Insert(post).Result;
             }).AuthorizeWith("AdminPolicy");

            Field<PostType>(
                "updatePost",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<PostInputType>> { Name = "post" }
             ),
             resolve: context =>
             {
                 var post = context.GetArgument<Post>("post");
                 return postRepository.Update(post).Result;
             }).AuthorizeWith("AdminPolicy");

            FieldAsync<BooleanGraphType>(
                "checkForSlug",
                arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "slug" }),
                resolve: async context => await postRepository.CheckIfSlugExists(context.GetArgument<string>("slug"))).AuthorizeWith("AdminPolicy");
        } 
    }
}
