﻿using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using QuCMS.API.GraphQLRelated.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated
{
    public class QuCMSMutation : ObjectGraphType
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IPostRepository _postRepository;

        public QuCMSMutation(IPostRepository postRepository, IHostingEnvironment hostingEnvironment)
        {
            _postRepository = postRepository;
            _hostingEnvironment = hostingEnvironment;

            Name = "Mutation";

            Field<PostRelatedType>("postRelated", resolve: context => new { });
        }
    }
}
