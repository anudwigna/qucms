﻿using GraphQL.Types;
using QuCMS.API.GraphQLRelated.Types;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuCMS.API.GraphQLRelated
{
    public class QuCMSQuery : ObjectGraphType
    {
        public QuCMSQuery
            (
                ICategoryRepository categoryRepository
            )
        {
            Field<CategoryRelatedType>("categoryRelated", resolve: context => new { });
            Field<AuthorRelatedType>("authorRelated", resolve: context => new { });
            Field<DifficultyLevelRelatedType>("difficultyLevelRelated", resolve: context => new { });
            Field<TagRelatedType>("tagRelated", resolve: context => new { });
            Field<PhotoRelatedType>("photoRelated", resolve: context => new { });
            Field<PostRelatedType>("postRelated", resolve: context => new { });
        }
    }
}
