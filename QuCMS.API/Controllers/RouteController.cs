﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;

namespace QuCMS.API.Controllers
{
    [EnableCors("AllowAllCORS")]
    [Route("api/[controller]")]
    [ApiController]
    public class RouteController : ControllerBase
    {
        private readonly IPostRepository _postRepository;
        public RouteController(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        [Route("all")]
        public async Task<IActionResult> All()
        {
            try
            {
                var posts =  await _postRepository.GetAll();
                var selectedPosts = posts.Select(p => new SelectedPost {
                    Id = p.Id,
                    Slug = p.Slug
                });
                return Ok(selectedPosts);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

    public class SelectedPost
    {
        public int Id { get; set; }
        public string Slug { get; set; }
    }
}