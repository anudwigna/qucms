﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using GraphQL.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuCMS.API.Filters;
using QuCMS.API.GraphQLRelated;

namespace QuCMS.API.Controllers
{
    [EnableCors("AllowAllCORS")]
    [Route("[controller]")]
    [ApiController]
    public class GraphQLController : ControllerBase
    {
        private readonly IDocumentExecuter _documentExecuter;
        private readonly ISchema _schema;
        IEnumerable<IValidationRule> _validationRules;

        public GraphQLController(ISchema schema, IDocumentExecuter documentExecuter, IEnumerable<IValidationRule> validationRules)
        {
            _schema = schema;
            _documentExecuter = documentExecuter;
            _validationRules = validationRules;
        }

        [HttpPost]
        //[ServiceFilter(typeof(RelayResourceFilter))]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            if (query == null) { throw new ArgumentNullException(nameof(query)); }
            var inputs = query.Variables.ToInputs();
            var executionOptions = new ExecutionOptions
            {
                Schema = _schema,
                Query = query.Query,
                Inputs = inputs,
                UserContext = new GraphQLUserContext
                {
                    User = User
                },
                ValidationRules = DocumentValidator.CoreRules().Concat(_validationRules).ToList()
        };

            var result = await _documentExecuter.ExecuteAsync(executionOptions).ConfigureAwait(false);

            if (result.Errors?.Count > 0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        
    }
}