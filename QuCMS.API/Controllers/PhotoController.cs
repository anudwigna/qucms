﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuCMS.API.Models;
using QuCMS.Data.Entities;
using QuCMS.Data.Repositories;
using SixLabors.ImageSharp;
using SixLabors.Primitives;

namespace QuCMS.API.Controllers
{
    [EnableCors("AllowAllCORS")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private IHostingEnvironment _hostingEnvironment;
        private IPhotoRepository _photoRepository;

        public PhotoController(IHostingEnvironment hostingEnvironment, IPhotoRepository photoRepository)
        {
            _hostingEnvironment = hostingEnvironment;
            _photoRepository = photoRepository;
        }

        [HttpPost]
        [Route("insert")]
        public async Task<IActionResult> Insert(IFormCollection data)
        {
            var uploadPath = Path.Combine(_hostingEnvironment.WebRootPath, "images");
            try
            {
                string filename = "", title = "", description = "";
                Photo photo = new Photo();

                if(data.ContainsKey("filename"))
                {
                    filename = data["filename"];
                }
                if (data.ContainsKey("title"))
                {
                    title = data["title"];
                }
                if (data.ContainsKey("description"))
                {
                    description = data["description"];
                }

                Random random = new Random();
                photo.Extension = Path.GetExtension(filename).ToLower();
                photo.Name = random.Next(1000, 1000000) + filename;
                photo.Description = description;
                photo.Title = title;

                if (data.Files[0].Length > 5 * 1024 * 1024)
                    throw new Exception("File Size exceeds 2MB");

                if (photo.Extension == ".png" || photo.Extension == ".jpg" || photo.Extension == ".jpeg")
                {
                    if (data.Files[0].Length > 0)
                    {
                        var filePath = Path.Combine(uploadPath, photo.Name);
                        using (var ms = new MemoryStream())
                        {
                            await data.Files[0].CopyToAsync(ms);
                            ms.Position = 0;
                            var image = Image.Load(ms);
                            image.Save(filePath);
                        }
                    }
                }
                else
                    throw new Exception("The file you are trying to upload is not an image.");

                _photoRepository.Insert(photo);

                return Ok("Success!");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}