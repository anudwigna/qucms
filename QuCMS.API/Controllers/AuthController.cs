﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QuCMS.API.Models;
using QuCMS.Data.AuthModels;

namespace QuCMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAllCORS")]
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public AuthController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [Route("test")]
        public string Test()
        {
            return "Test";
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login([FromBody] LoginVM model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);

            if (result.Succeeded)
            {
                var appUser = _userManager.Users.SingleOrDefault(r => r.UserName == model.Username);
                var theToken = new TheToken()
                {
                    AccessToken = GenerateJwtToken(model.Username, appUser).ToString(),
                    ExpiresIn = Convert.ToInt32(_configuration["JwtExpireDays"]) * 24 * 60
                };
                return Ok(theToken);
            }
            else
            {
                return BadRequest("Invalid Username or Password!");
            }
            throw new ApplicationException("INVALID_LOGIN_ATTEMPT");
        }

        [Authorize]
        [HttpGet]
        [Route("user")]
        public async Task<ActionResult<ApplicationUser>> GetUser()
        {
            try
            {
                var userId = User.Claims.Where( c => c.Type == "sub").FirstOrDefault();
                var theUser = await _userManager.FindByEmailAsync(userId.Value);
                theUser.PasswordHash = null;
                theUser.SecurityStamp = null;
                theUser.ConcurrencyStamp = null;
                return Ok(new { result = theUser });
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //[Authorize]
        //[HttpPost]
        //[Route("register")]
        //public async Task<ActionResult> Register(RegisterVM model)
        //{
        //    var user = new ApplicationUser
        //    {
        //        UserName = model.Username,
        //        Email = model.Username
        //    };

        //    var theUser = await _userManager.FindByNameAsync(model.Username);

        //    if (theUser != null)
        //    {
        //        _apiResponse.Error = "This user is already registered!";
        //        return BadRequest(_apiResponse);
        //    }

        //    if (model.Password == model.ConfirmPassword)
        //    {
        //        var result = await _userManager.CreateAsync(user, model.Password);

        //        if (result.Succeeded)
        //        {
        //            await _signInManager.SignInAsync(user, false);
        //            _apiResponse.Result = new Token()
        //            {
        //                AccessToken = GenerateJwtToken(model.Username, user).ToString(),
        //                ExpiresIn = Convert.ToInt32(_configuration["JwtExpireDays"]) * 24 * 60
        //            };
        //            return Ok(_apiResponse);
        //        }
        //        else
        //        {
        //            _apiResponse.Error = "Cannot register this User!";
        //            return BadRequest(_apiResponse);
        //        }
        //    }

        //    throw new ApplicationException("CANNOT_REGISTER_USER");
        //}

        private object GenerateJwtToken(string email, ApplicationUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim("role", "Admin")
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}