const axios = require('axios');
const fs = require('fs-extra');

module.exports = function scraper() {
    const writeData = (path, data) => {
      return new Promise((resolve, reject) => {
        try {
          fs.ensureFileSync(path)
          fs.writeJson(path, data, resolve(`${path} Write Successful`) )
        } catch (e) {
          console.error(`${path} Write Failed. ${e}`)
          reject(`${path} Write Failed. ${e}`)  
        }
      })
    }
  
    // Add hook for build
    this.nuxt.hook('build:before', async builder => {
  
      // Clean data directory
      fs.emptyDir('static/data')
  
      // Empty array to fill with promises
      const scraper = []
  
      // One of these for every top level page, a loop for dynamic nested pages
      scraper.push(writeData('static/data/index.json', await fetchLatestPosts()))
  
      // Finish when all of them are done
      return Promise.all(scraper).then(() => {
        console.log('JSON Build Complete!')
      }).catch(err => {
        console.error(err)
      });
    });
}

async function fetchLatestPosts(){
    let query = `postRelated{
      posts{
        id title slug excerpt publishedDate htmlContent
        category{
          id name
        }
        difficultyLevel{
          id name
        }
        tags{
          id name
        }
        metas{
          id name content
        }
      }
    }`;
    let response =  await axios.post("https://qucms.qubex.com.np/graphql",{"query": `{${query}}`});
    return response.data.data.postRelated.posts;
}