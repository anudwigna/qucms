const pkg = require('./package')
const axios = require('axios')
//const allRouteUrl = 'http://localhost:50425/api/route/all';
//const graphQLUrl = 'http://localhost:50425/graphql';
const allRouteUrl = 'https://qucms.qubex.com.np/api/route/all';
const graphQLUrl = 'https://qucms.qubex.com.np/graphql';
//const allRouteUrl = 'http://182.93.85.219:8041/api/route/all';
//const graphQLUrl = 'http://182.93.85.219:8041/graphql/';

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
  mode: 'universal',
  generate: {
    routes: function () {
      return axios.get(allRouteUrl)
      .then((res) => {
        return res.data.map((route) => {
          return `/article/${route.id}/${route.slug}`
        })
      })
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.description,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Abhinandan Aryal\'s Blog' },
      { hid: 'apple-mobile-web-app-title', name: 'apple-mobile-web-app-title', content: 'Abhinandan Aryal\'s Blog' },
      { hid: 'author', name: 'author', content: 'Abhinandan Aryal' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Abhinandan Aryal\'s Blog' },
      { hid: 'og:image', property: 'og:image', content: 'https://abhinandanaryal.info.np/abhi_photo.jpg' },
      { hid: 'twitter:image', name: 'twitter:image', content: 'https://abhinandanaryal.info.np/abhi_photo.jpg' },
    ],
    // meta: [
    //   { "http-equiv": "Content-Security-Policy", content: "default-src 'self'; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; font-src 'self' https://fonts.gstatic.com; script-src * 'sha256-4Y8cS2TcgfGVt/zCkP/ect+r0+V4o3+b/md8sqMhW50=' 'sha256-zwjON4XFzR70m1RvN6b0KBNrSTnuC9Yko18X/2l87P4=';img-src 'self' https://i.stack.imgur.com/ https://qucms.qubex.com.np/; connect-src 'self' https://qucms.qubex.com.np/graphql http://localhost:50425/graphql;"},
    // ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'image_src', href: 'https://abhinandanaryal.info.np/abhi_photo.jpg' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      },
      {
        rel:'stylesheet',
        href:'https://fonts.googleapis.com/css?family=Inconsolata'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '~/assets/style/all.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify'
  ],

  
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/apollo',
    '@nuxtjs/sitemap',
    '~/modules/scrape',
    '@nuxtjs/google-analytics'
  ],

  googleAnalytics: {
    id: 'UA-136693327-1'
  },
  
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://abhinandanaryal.info.np',
    cacheTime: 1000 * 60 * 15,
    gzip: true,
    generate: true, // Enable me when using nuxt generate
    exclude: [
      '/secret',
      '/admin/**'
    ],
    routes () {
      return axios.get(allRouteUrl)
      .then(res => res.data.map(route =>  `/article/${route.id}/${route.slug}`))
    }
  },

  /*
  ** Apollo module configuration
  */
  apollo:{
    tokenName: 'auth._token.local',
    authenticationType: '',
    clientConfigs:{
      default:{
        httpEndpoint: graphQLUrl,
        fetchPolicy: 'no-cache',
      }
    }
  },
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ["~assets/style/variables.styl"]
      }
    },
    
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  }
}
