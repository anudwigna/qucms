import allData from '../static/data/index.json'

export const state = () => ({
    latestPosts: [],
    post:{},
    allPosts:[]
})
  
export const mutations = {
    setLatestPosts (state) {
        state.latestPosts = state.allPosts.slice(0, 20);
    },
    setPost (state, postId) {
        state.post = state.allPosts.find(post => post.id == postId);
    },
    setAllPosts(state){
        state.allPosts = allData;
    }
}